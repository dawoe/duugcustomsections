# DUUG Custom sections code #

To run this you have to make the following changes to the web.config: 

* Clear the umbracoConfigurationStatus app setting
* On the umbracoDbDSN connectionstring clear the connectionString and providerName property

After this you can run the website and it will start the Umbraco installer. All tables needed for the custom section are created by the application

If you have any questions contact me on twitter : @dawoe21