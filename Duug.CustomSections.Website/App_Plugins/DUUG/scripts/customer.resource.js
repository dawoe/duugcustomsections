﻿angular.module('umbraco.resources').factory('customerResource', function ($http) {
    return {
        getCustomer: function(id) {
            return $http.get("backoffice/DUUG/Customers/GetById/" + id);
        },
        saveCustomer: function(person) {
            return $http.post("backoffice/DUUG/Customers/Save", angular.toJson(person));
        },
        deleteCustomer: function(person) {
            return $http.delete("backoffice/DUUG/Customers/Delete", angular.toJson(person));
        },
        deleteCustomerById : function(id) {
            return $http.delete("backoffice/DUUG/Customers/DeleteById/" + id);
        }
    };
});