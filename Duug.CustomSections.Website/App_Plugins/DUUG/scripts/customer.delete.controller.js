﻿angular.module('umbraco').controller('DUUG.CustomerDeleteController',
    function ($scope, $routeParams, customerResource, notificationsService, navigationService,$location,treeService) {

        $scope.Delete = function(id) {
            var customerid = id.replace('customer-', '');

            customerResource.deleteCustomerById(customerid).then(function () {
                navigationService.hideNavigation();               
                treeService.removeNode($scope.currentNode);
                $location.path('duug-customers');
                notificationsService.success("Success", "Customer has been deleted");
            });
        };

        $scope.Cancel = function() {
            navigationService.hideNavigation();
        };
    });