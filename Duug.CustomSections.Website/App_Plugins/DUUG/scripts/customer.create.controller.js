﻿angular.module('umbraco').controller('DUUG.CustomerCreateController',
    function($scope, $routeParams, customerResource, notificationsService,navigationService,$location) {
        $scope.customer = { Id: 0, Name: '', Street: '', City: '', HouseNumber: '', ZipCode: '', Email: '', PhoneNumber: '' };


        $scope.save = function (customer) {
            customerResource.saveCustomer(customer).then(function (response) {
                $scope.customer = response.data;
                var pathArray = ['-1'];               
                pathArray.push('customer-' + $scope.customer.Id);
                navigationService.syncTree({ tree: 'duug-customers-tree', path: pathArray, forceReload: true, activate: true }).then(
                    function(syncArgs) {
                        $location.path(syncArgs.node.routePath);
                    });
                navigationService.hideNavigation();
                notificationsService.success("Success", customer.Name + " has been created");
            });
        };
    });