﻿angular.module('umbraco').controller('DUUG.CustomerEditController',
    function ($scope, $routeParams, customerResource, notificationsService,navigationService) {
        $scope.loaded = false;

        $scope.tabs = [{ id: "Content", label: "Content" }, {id:"Logo", label:"Logo"}];

       if ($routeParams.id == -1) {
            $scope.customer = {};
            $scope.loaded = true;
        }
        else {
           
            customerResource.getCustomer($routeParams.id).then(function (response) {
                $scope.customer = response.data;                
                 
                $scope.logo = [{
                    alias: 'logo',
                    label: 'Customer Logo',
                    description: 'Choose the logo from the media library',
                    view: 'mediapicker',
                    config: {
                        multiPicker: "0",
                        startNodeId: -1
                    },
                    value: $scope.customer.Logo.toString()
                    }];
                $scope.loaded = true;                
            });
        }

       $scope.save = function (customer) {
            if ($scope.customerForm.$invalid) {
                return;
            }
            if ($scope.logo[0].value === '') {
                customer.Logo = -1;
            } else {
                customer.Logo = $scope.logo[0].value;
            }
           
            customerResource.saveCustomer(customer).then(function (response) {
                $scope.customer = response.data;
                var pathArray = ['-1'];
                pathArray.push('customer-' + $scope.customer.Id);
                navigationService.syncTree({ tree: 'duug-customers-tree', path: pathArray, forceReload: true, activate: true });                
                notificationsService.success("Success", customer.Name + " has been saved");
                $scope.customerForm.$dirty = false;
            });
        };
    });