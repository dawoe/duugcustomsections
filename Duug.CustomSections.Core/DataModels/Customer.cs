﻿namespace Duug.CustomSections.Core.DataModels
{
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    [TableName(Constants.CustomerTable)]
    [PrimaryKey("Id")]
    public class Customer
    {
        [PrimaryKeyColumn(AutoIncrement = true)]
        public int Id { get; set; }

        [Length(100)]
        [NullSetting(NullSetting = NullSettings.NotNull)]
        [Column("CustomerName")]
        public string Name { get; set; }

        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Street { get; set; }

        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string City { get; set; }

        [Length(10)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string ZipCode { get; set; }

        [Length(10)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string HouseNumber { get; set; }

        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Email { get; set; }

        [Length(20)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string PhoneNumber { get; set; }

        [NullSetting(NullSetting = NullSettings.Null)]
        public int Logo { get; set; }
    }
}
