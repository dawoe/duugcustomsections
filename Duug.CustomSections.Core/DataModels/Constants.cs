﻿namespace Duug.CustomSections.Core.DataModels
{
    public class Constants
    {
        public const string CustomerTable = "DUUG_Customers";

        public const string ContactPersonTable = "DUUG_ContactPersons";
    }
}
