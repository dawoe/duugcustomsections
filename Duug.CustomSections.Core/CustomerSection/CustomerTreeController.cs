﻿namespace Duug.CustomSections.Core.CustomerSection
{
    using System.Globalization;
    using System.Net.Http.Formatting;
    using System.Web.UI.WebControls;

    using Duug.CustomSections.Core.WebApi;

    using umbraco;
    using umbraco.BusinessLogic.Actions;
    using Umbraco.Core;
    using Umbraco.Web;
    using Umbraco.Web.Models.Trees;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.Trees;

    using MenuItem = Umbraco.Web.Models.Trees.MenuItem;
    using MenuItemCollection = Umbraco.Web.Models.Trees.MenuItemCollection;
    using TreeNodeCollection = Umbraco.Web.Models.Trees.TreeNodeCollection;

    [Tree("duug-customers", "duug-customers-tree", "Customers")]
    [PluginController("DUUG")]
    public class CustomerTreeController : TreeController
    {
        private const string CustomerPrefix = "customer-";

        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            var nodes = new TreeNodeCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                var ctrl = new CustomersController();

                var customerRoute = string.Format("{0}{1}/edit/", queryStrings.GetValue<string>("application"), this.TreeAlias.EnsureStartsWith('/'));

                foreach (var customer in ctrl.GetAll())
                {
                    var node = CreateTreeNode(
                        string.Format("{0}{1}", CustomerPrefix, customer.Id.ToString(CultureInfo.InvariantCulture)),
                        Constants.System.Root.ToInvariantString(),
                        queryStrings,
                        customer.Name,
                        "icon-client",
                        false,
                        string.Format("{0}{1}", customerRoute, customer.Id));                    

                    nodes.Add(node);
                }
            }                      

            return nodes;
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menu = new MenuItemCollection();


            if (id == Constants.System.Root.ToInvariantString())
            {
               // root actions                                     
               menu.Items.Add(
                    new MenuItem("create", ui.Text("actions", ActionNew.Instance.Alias))
                        {
                            Icon
                                =
                                "add"
                        });
                menu.Items.Add<RefreshNode, ActionRefresh>(ui.Text("actions", ActionRefresh.Instance.Alias), true);
                return menu;
            }

            if (id.StartsWith(CustomerPrefix))
            {
                menu.Items.Add(new MenuItem("delete", ui.Text("actions", ActionDelete.Instance.Alias))
                                   {
                                       Icon = "delete"
                                   });
            }

            return menu;
        }
    }
}
