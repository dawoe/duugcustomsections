﻿namespace Duug.CustomSections.Core.WebApi
{
    using System.Collections.Generic;
    using System.Web.Http;

    using Duug.CustomSections.Core.DataModels;

    using Umbraco.Core;
    using Umbraco.Core.Persistence;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.WebApi;

    [PluginController("DUUG")]
    public class CustomersController : UmbracoAuthorizedApiController
    {     
        [HttpGet]
        public IEnumerable<DataModels.Customer> GetAll()
        {
            var query = new Sql().From<DataModels.Customer>().OrderBy<DataModels.Customer>(x => x.Name);
            return DatabaseContext.Database.Fetch<DataModels.Customer>(query);
        }

        [HttpGet]
        public DataModels.Customer GetById(int id)
        {
            var query = new Sql().From<DataModels.Customer>().Where<DataModels.Customer>(x => x.Id == id);
            return DatabaseContext.Database.FirstOrDefault<DataModels.Customer>(query);
        }

        [HttpPost]
        public DataModels.Customer Save(DataModels.Customer customer)
        {
            if (DatabaseContext.Database.IsNew(customer))
            {
                DatabaseContext.Database.Save(customer);
            }
            else
            {
                DatabaseContext.Database.Update(customer);
            }

            return customer;
        }

        [HttpDelete]
        public int Delete(DataModels.Customer customer)
        {
            return DatabaseContext.Database.Delete(customer);
        }

        [HttpDelete]
        public int DeleteById(int id)
        {
            return DatabaseContext.Database.Delete<Customer>(id);
        }
    }
}
