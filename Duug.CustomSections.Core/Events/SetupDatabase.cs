﻿namespace Duug.CustomSections.Core.Events
{
    using umbraco.cms.businesslogic.packager;

    using Umbraco.Core;
    using Umbraco.Core.Persistence;

    public class SetupDatabase : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var db = applicationContext.DatabaseContext.Database;

            if (!db.TableExist(DataModels.Constants.CustomerTable))
            {
                db.CreateTable<DataModels.Customer>(false);
            }            
        }
    }
}
